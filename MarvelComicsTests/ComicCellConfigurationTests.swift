// Created by Scott Symes

import XCTest
@testable import MarvelComics

final class ComicCellConfigurationTests: XCTestCase {

	var testSubject: ComicCellConfiguration!

	override func setUp() {
		super.setUp()
		let comic: Comic = .initiative10223
		let images: [URL: UIImage] = .comics
		let url = comic.thumbnail.url(.portraitFantastic)!
		testSubject = ComicCellConfiguration(comic: comic, image: images[url]!)
	}

	override func tearDown() {
		super.tearDown()
		testSubject = nil
	}

	func testConfigurationWithImage() {
		XCTAssertEqual(testSubject.issueNumber, "Issue #35")
		XCTAssertEqual(testSubject.title, "Marvel Premiere (1972) #35")
		XCTAssertEqual(testSubject.displayDate, "Apr 5, 2023")
		XCTAssertEqual(testSubject.image, UIImage(imageLiteralResourceName: "Premiere35"))
	}

	func testConfigurationWithoutImage() {
		testSubject = .init(comic: .initiative10223, image: nil)
		XCTAssertEqual(testSubject.issueNumber, "Issue #35")
		XCTAssertEqual(testSubject.title, "Marvel Premiere (1972) #35")
		XCTAssertEqual(testSubject.displayDate, "Apr 5, 2023")
		XCTAssertNil(testSubject.image)
	}

}
