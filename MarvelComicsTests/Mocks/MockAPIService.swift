// Created by Scott Symes

import Foundation
@testable import MarvelComics

final class MockAPIService<T: Codable>: APIProtocol {

	let items: [T]

	init(items: [T]) {
		self.items = items
	}

	func getItems<T>(_ endpoint: APIEndpoint) async throws -> [T] where T : Decodable, T : Encodable {
		(items as? [T]) ?? []
	}

}
