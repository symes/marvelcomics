// Created by Scott Symes

import Foundation
import SwiftUI
@testable import MarvelComics

class MockImageService: ImageServiceProtocol {

	@Published var images: [URL: UIImage]

	private(set) var queriedURLs: [URL] = []

	var imagesPublisher: Published<[URL: UIImage]>.Publisher {
		$images
	}

	init(images: [URL: UIImage]) {
		self.images = images
	}

	func fetchImages(for urls: [URL]) {
		queriedURLs = urls
	}

}
