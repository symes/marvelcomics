// Created by Scott Symes

import XCTest
@testable import MarvelComics

final class APIServiceTests: XCTestCase {

	var testSubject: APIService!

	override func setUp() {
		super.setUp()
		let configuration = URLSessionConfiguration.default
		configuration.protocolClasses = [MockURLProtocol.self]
		let urlSession = URLSession.init(configuration: configuration)

		testSubject = APIService(urlSession: urlSession)
	}

	override func tearDown() {
		super.tearDown()
		testSubject = nil
	}

	func testDecodingCharacters() throws {
		// GIVEN some JSON data from developer.marvel
		let data = try XCTUnwrap(Self.getJSONFile("characters"))
		do {
			// WHEN the JSON is decoded
			let decoded: [MarvelCharacter] = try testSubject.decodeAPIResult(data)
			XCTAssertEqual(decoded.count, 20)
		} catch {
			// THEN no errors should be thrown
			XCTFail("Decoding failed \(error)")
		}
	}

	func testDecodingComics() throws {
		// GIVEN some JSON data from developer.marvel
		let data = try XCTUnwrap(Self.getJSONFile("comics1011334"))
		do {
			// WHEN the JSON is decoded
			let decoded: [Comic] = try testSubject.decodeAPIResult(data)
			XCTAssertEqual(decoded.count, 12)
		} catch {
			// THEN no errors should be thrown
			XCTFail("Decoding failed \(error)")
		}
	}

	func testGetItemsCharacters() async throws {
		// GIVEN the URLSession is configured to return a valid response & data
		MockURLProtocol.requestHandler = { request in
			// THEN the correct API endpoint is called
			let urlComponents = try XCTUnwrap(URLComponents(url: XCTUnwrap(request.url), resolvingAgainstBaseURL: true))
			XCTAssertEqual(urlComponents.path, "/v1/public/characters")
			// AND there are 3 Query Parameters
			XCTAssertEqual(urlComponents.queryItems?.count, 3)
			XCTAssertEqual(urlComponents.queryItems?.first?.name, "ts")
			XCTAssertEqual(urlComponents.queryItems?[1].name, "apikey")
			XCTAssertEqual(urlComponents.queryItems?.last?.name, "hash")
			let response = HTTPURLResponse(url: request.url!,
										   statusCode: 200,
										   httpVersion: nil,
										   headerFields: ["Content-Type": "application/json"])!
			let data = Self.getJSONFile("characters")!
			return (response, data)
		}
		// WHEN the characters endpoint is called
		let characters: [MarvelCharacter] = try await testSubject.getItems(.characters)

		// THEN no errors are thrown and all characters are decoded
		XCTAssertEqual(characters.count, 20)
	}

	func testGetItemsComics() async throws {
		// GIVEN the URLSession is configured to return a valid response & data
		MockURLProtocol.requestHandler = { request in
			// THEN the correct API endpoint is called
			let urlComponents = try XCTUnwrap(URLComponents(url: XCTUnwrap(request.url), resolvingAgainstBaseURL: true))
			XCTAssertEqual(urlComponents.path, "/v1/public/characters/1011334/comics")
			// AND there are 3 Query Parameters
			XCTAssertEqual(urlComponents.queryItems?.count, 3)
			XCTAssertEqual(urlComponents.queryItems?.first?.name, "ts")
			XCTAssertEqual(urlComponents.queryItems?[1].name, "apikey")
			XCTAssertEqual(urlComponents.queryItems?.last?.name, "hash")
			let response = HTTPURLResponse(url: request.url!,
										   statusCode: 200,
										   httpVersion: nil,
										   headerFields: ["Content-Type": "application/json"])!
			let data = Self.getJSONFile("comics1011334")!
			return (response, data)
		}

		// WHEN the characters endpoint is called
		let comics: [Comic] = try await testSubject.getItems(.comics(.threeDeeMan))

		// THEN no errors are thrown and all characters are decoded
		XCTAssertEqual(comics.count, 12)
	}

}

extension APIServiceTests {

	static func getJSONFile(_ name: String) -> Data? {
		guard let url = Bundle(for: Self.self).url(forResource: name, withExtension: "json") else { return nil }
		guard let data = try? Data(contentsOf: url) else { return nil }
		return data
	}

}
