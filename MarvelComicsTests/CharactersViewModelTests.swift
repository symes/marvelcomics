// Created by Scott Symes

import Combine
import XCTest
@testable import MarvelComics

@MainActor
final class CharactersViewModelTests: XCTestCase {

	var apiService: MockAPIService<MarvelCharacter>!
	var imageService: MockImageService!
	var testSubject: CharactersViewModel!
	var cancellables: Set<AnyCancellable> = .init()

	override func setUp() {
		super.setUp()
		// set up api service to return array of all marvel characters
		apiService = MockAPIService(items: MarvelCharacter.all)

		// set up image service to have all the images for these characters loaded
		imageService = MockImageService(images: .characters)

		testSubject = CharactersViewModel(
			apiService: apiService,
			imageService: imageService
		)
	}

	override func tearDown() {
		super.tearDown()
		apiService = nil
		imageService = nil
		testSubject = nil
	}

	func testFetchCharacters() {
		// GIVEN that we have an api service that will return an array of `MarvelCharacter`s
		let expectation = expectation(description: "fetchedCharacters")

		testSubject.$characters
			.dropFirst()
			.sink {
				// THEN all the characters should be populated into the testSubject
				XCTAssertEqual($0, MarvelCharacter.all.sorted { $0.comics.count > $1.comics.count })
				expectation.fulfill()
			}
			.store(in: &cancellables)

		// WHEN we call `fetchCharacterList`
		testSubject.fetchCharacterList()
		wait(for: [expectation], timeout: 2)

		// AND there should be no error
		XCTAssertNil(testSubject.error)
	}

	func testFetchImages() {
		// GIVEN that we have an image service that will return a dictionary of images
		let expectation = expectation(description: "fetchedImages")

		testSubject.$images
			.sink { _ in
				expectation.fulfill()
			}
			.store(in: &cancellables)

		// WHEN we call `fetchCharacterList`
		testSubject.fetchCharacterList()
		wait(for: [expectation], timeout: 2)

		// THEN the URLs passed in should be all those from our characters
		XCTAssertEqual(
			imageService.queriedURLs.sorted {
				// sort both arrays to ensure order is not an issue with passing the test
				$0.absoluteString < $1.absoluteString
			},
			testSubject.characters
				.compactMap(\.thumbnailResource)
				.compactMap { $0.url(.fullSize) }
				.sorted { $0.absoluteString < $1.absoluteString }
		)

		// AND all of the images should be available for each character
		for character in MarvelCharacter.all {
			XCTAssertNotNil(testSubject.image(for: character))
		}

		// AND there should be no error
		XCTAssertNil(testSubject.error)
	}

}
