![MARVEL logo][MARVEL]


![3D-Man][3D-Man]{width=20%} ![A-Bomb][A-Bomb]{width=20%} ![Adam Warlock][AdamWarlock]{width=20%} ![Absorbing Man][AbsorbingMan]{width=20%}


#  Marvel Comics Character Explorer (tvOS)

This is a coding exercise to create a tvOS app following the provided [guide][guide], fetching data from the `developer.marvel.com` API. This app lists Marvel characters and each list-item can navigate to a detail screen showing a list of that character's comics.

### API Keys

Access to the `developer.marvel.com` API requires supplying a pair of API keys. These should be added to a file `MarvelComics/Debug.xcconfig`. An example of the format is `MarvelComics/Example.xcconfig`.

### Placeholders & Tech Debt

There are a few non-functional / placeholder UI elements that are just left as tech debt in the current implementation:
- Character List Screen - "See All ›" button
- Character Detail Screen - "☆ Follow Character" button

Also to do:
- Error handling and logging
- Analytics (if required)
- Paging of API results
- Caching images to disk. Respecting [cache limits][Attribution] of ~24 hours
- Cache data and save bandwidth by implementing [`etag` attribute][API-general] in URLRequest header and accepting gzipped responses.
- Use a package like `SwiftGen` to move away from `String`ly typed resource references for thing like:
	- System Images
	- Bundled Images
	- Localization Strings

There are also numerous comments throughout the code related to small improvements

The Marvel logo (used for the app icon) is taken from [Wikipedia][Marvel-logo-source]

## Screenshots

![Character Listing][Screen-Listing]
Character Listing Screen


![Character Detail][Screen-Detail]
Character Detail / Comic Listing Screen




[guide]: DE&ET_coding_exercise_(2)_for_Sr_SWE_Frank_Manni.pdf

[MARVEL]: MarvelComics/Resources/Assets.xcassets/App&#32;Icon&#32;&&#32;Top&#32;Shelf&#32;Image.brandassets/Top&#32;Shelf&#32;Image&#32;Wide.imageset/Marvel_logo_2320x720.png

[Marvel-logo-source]: https://en.wikipedia.org/wiki/File:Marvel_Logo.svg

[AbsorbingMan]: MarvelComics/Resources/Preview&#32;Content/Preview&#32;Assets.xcassets/1009148_Absorbing-Man.imageset/1009148_Absorbing-Man.jpg

[AdamWarlock]: MarvelComics/Resources/Preview&#32;Content/Preview&#32;Assets.xcassets/1010354_Adam_Warlock.imageset/1010354_Adam_Warlock.jpg

[3D-Man]: MarvelComics/Resources/Preview&#32;Content/Preview&#32;Assets.xcassets/1011334_3D-Man.imageset/1011334_3D-Man.jpg

[A-Bomb]: MarvelComics/Resources/Preview&#32;Content/Preview&#32;Assets.xcassets/1017100_A-Bomb.imageset/1017100_A-Bomb.jpg

[Attribution]: https://developer.marvel.com/documentation/attribution

[API-general]: https://developer.marvel.com/documentation/generalinfo

[Screen-Listing]: img/CharacterListing.png

[Screen-Detail]: img/DetailSelected.png