// Created by Scott Symes

import Foundation

extension KeyedDecodingContainer {

	func decodeIfPresentNotEmpty<Value: Decodable & Collection>(forKey key: KeyedDecodingContainer<K>.Key) throws -> Value? {
		let collection = try self.decodeIfPresent(Value.self, forKey: key)
		guard let collection else { return nil }
		return collection.isEmpty ? nil : collection
	}

}
