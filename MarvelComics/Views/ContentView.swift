// Created by Scott Symes

import SwiftUI

struct ContentView: View {

	@ObservedObject var viewModel: CharactersViewModel

	var body: some View {
		NavigationStack {
			VStack {
				Spacer()

				HStack {
					viewModel.heroImage
					Spacer()
				}

				HStack(alignment: .bottom) {
					Text(viewModel.listHeadingText)
						.font(.headline)
					Spacer()

					// This button is intentionally non-functional.
					// It could be used to navigate to a LazyVGrid of all characters
					// but is currently outside the scope of this project
					Button(action: { /* no-op */ }, label: {
						HStack {
							Text(viewModel.seeAllButtonText)
								.font(.caption)
							Image(systemName: "chevron.right")
						}
						.disabled(true)
					})
				}
					.frame(height: 100)
					.fixedSize(horizontal: false, vertical: true)

				ScrollView(.horizontal, showsIndicators: false) {
					LazyHStack {
						ForEach(viewModel.characters) { character in
							NavigationLink(value: character) {
								let image = viewModel.image(for: character)
								CharacterCell(character: character, image: image)
									.frame(width: 250, height: 400)
									.fixedSize(horizontal: true, vertical: true)
							}
							.frame(width: 300, height: 400)
						}
					}
					.padding(.vertical, 50)
					.navigationDestination(for: MarvelCharacter.self) {
						let viewModel = CharacterDetailViewModel(
							character: $0,
							apiService: viewModel.apiService,
							imageService: viewModel.imageService
						)
						CharacterView(viewModel: viewModel, isFollowing: false)
					}
				}
					.fixedSize(horizontal: false, vertical: true)

				Spacer()
				HStack {
					Spacer()
					Text(viewModel.attributionText)
						.font(.system(size: 10))
				}
			}
		}
		.task {
			viewModel.fetchCharacterList()
		}
	}

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
		ContentView(viewModel: .init(
			apiService: PreviewAPIService(),
			imageService: PreviewImageService(.characters)
		))
    }
}
