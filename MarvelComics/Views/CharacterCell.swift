// Created by Scott Symes

import SwiftUI

struct CharacterCell: View {

	let character: MarvelCharacter
	let image: UIImage?

	private let width: CGFloat = 250

    var body: some View {
		VStack {
			if let image {
				Image(uiImage: image)
					.resizable()
					.aspectRatio(contentMode: .fill)
					.frame(width: width, height: width)
					.clipShape(
						Circle()
					)
			} else {
				ProgressView()
					.frame(width: width, height: width)
			}
			Text(character.name)
				.frame(maxWidth: width)
				.lineLimit(2)
				.fixedSize(horizontal: false, vertical: true)
			Spacer()
		}
    }

}

struct CharacterCell_Previews: PreviewProvider {

    static var previews: some View {
		CharacterCell(character: .threeDeeMan, image: nil)
			.previewDisplayName("Loading")

		CharacterCell(
			character: .threeDeeMan,
			image: UIImage(imageLiteralResourceName: "1011334_3D-Man")
		).previewDisplayName("3-D Man")

		CharacterCell(
			character: .aBomb,
			image: UIImage(imageLiteralResourceName: "1017100_A-Bomb")
		).previewDisplayName("A-Bomb")
    }

}
