// Created by Scott Symes

import SwiftUI

struct CharacterView: View {

	@ObservedObject var viewModel: CharacterDetailViewModel

	// This is just a placeholder to show the button function,
	// so not connected to any API & not stored in any model
	@State var isFollowing: Bool

    var body: some View {
		if let image = viewModel.thumbnailImage {
			ZStack {
				VStack {
					HStack {
						BackgroundImage(image: image)
						VStack(alignment: .leading) {
							Text(viewModel.character.name)
								.font(.title3)
								.lineLimit(2)
							FollowButton(isFollowing: $isFollowing) {
								isFollowing.toggle()
							}
							.fixedSize(horizontal: true, vertical: false)
						}
						.padding(.trailing, 50)
						.fixedSize(horizontal: true, vertical: false)
					}
					.frame(maxHeight: 350)
					Spacer()
					HStack {
						Spacer()
						Text(viewModel.attributionText)
							.font(.system(size: 10))
					}
				}.ignoresSafeArea()
				VStack {
					Spacer()
					if viewModel.allImagesLoaded {
						ScrollView(.horizontal, showsIndicators: false) {
							LazyHStack(alignment: .bottom) {
								ForEach(viewModel.comics) { comic in
									NavigationLink(value: comic) {
										ComicCell(configuration:
												.init(
													comic: comic,
													image: viewModel.image(for: comic)
												)
										)
									}
								}
							}
							.padding(.vertical, 20)
							.frame(height: 520)
						}
					} else {
						ProgressView()
							.frame(height: 400)
					}
				}
			}
		} else {
			ProgressView()
		}
    }

}

struct BackgroundImage: View {

	let image: UIImage

	var body: some View {
		Image(uiImage: image)
			.resizable()
			.aspectRatio(contentMode: .fill)
			.frame(width: .infinity, height: .infinity)
			.ignoresSafeArea()
			.mask(LinearGradient(gradient: Gradient(colors: [.black, .black, .clear]), startPoint: .leading, endPoint: .trailing))
			.mask(LinearGradient(gradient: Gradient(colors: [.black, .black, .black, .black, .black, .clear]), startPoint: .top, endPoint: .bottom))
	}

}

struct FollowButton: View {

	@Binding var isFollowing: Bool
	var configuration: CharacterDetailViewModel.FollowButtonConfiguration {
		.init(isFollowing: self.isFollowing)
	}
	var action: () -> Void

	var body: some View {
		Button(role: nil, action: action) {
			HStack {
				configuration.buttonImage
				Text(configuration.buttonText)
					.font(.subheadline)
					.lineLimit(1)
				Spacer()
			}
			.frame(width: 500)
			.fixedSize(horizontal: true, vertical: false)
		}
	}

}

struct CharacterView_Previews: PreviewProvider {

	@State static var isFollowing = false

    static var previews: some View {
		CharacterView(
			viewModel: .init(
				character: .threeDeeMan,
				apiService: PreviewAPIService(),
				imageService: PreviewImageService(.all)
			),
			isFollowing: false
		).previewDisplayName("3-D Man")

		CharacterView(
			viewModel: .init(
				character: .aBomb,
				apiService: PreviewAPIService(),
				imageService: PreviewImageService(.all)
			),
			isFollowing: true
		).previewDisplayName("A-Bomb")

		FollowButton(isFollowing: $isFollowing) {
			isFollowing.toggle()
		}.previewDisplayName("Follow Button")
    }
}
