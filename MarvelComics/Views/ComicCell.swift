// Created by Scott Symes

import SwiftUI

struct ComicCell: View {

	let configuration: ComicCellConfiguration

	let width: CGFloat = 280

    var body: some View {
		VStack {
			if let image = configuration.image {
				Image(uiImage: image)
					.resizable()
					.aspectRatio(contentMode: .fit)
					.frame(maxWidth: width)
			} else {
				ProgressView()
					.frame(height: width * (3/2)) // from `ResourceURL.ImageSize.portrait*`
					.fixedSize()
			}

			Text(configuration.title)
				.lineLimit(2)
				.multilineTextAlignment(.center)
				.font(.subheadline)
				.frame(width: width, alignment: .center)

			Text(configuration.issueNumber)
				.font(.caption)

			if let date = configuration.displayDate {
				Text(date)
					.font(.caption)
			}
		}
    }
}

struct ComicCell_Previews: PreviewProvider {
    static var previews: some View {
		ComicCell(configuration:
			.init(
				comic: .initiative22506,
				image: UIImage(imageLiteralResourceName: "Initiative16-1")
			)
		)
    }
}
