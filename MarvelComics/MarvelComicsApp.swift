// Created by Scott Symes

import SwiftUI

@main
struct MarvelComicsApp: App {
    var body: some Scene {
        WindowGroup {
			ContentView(viewModel: .init())
        }
    }
}
