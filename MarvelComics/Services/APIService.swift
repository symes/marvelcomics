// Created by Scott Symes

import CryptoKit
import Foundation

protocol APIProtocol {
	func getItems<T: Codable>(_ endpoint: APIEndpoint) async throws -> [T]
}

enum APIEndpoint {
	case characters
	case comics(MarvelCharacter)

	var path: String {
		switch self {
		case .characters:
			return "characters"
		case .comics(let character):
			return "characters/\(character.id)/comics"
		}
	}
}

/// Service to interact with API decribed at: https://developer.marvel.com/docs
final class APIService: APIProtocol {

	enum APIError: Error {
		case invalidResponse(URLResponse)
		case decodingError(Data, DecodingError)
	}

	struct CodedError: Error, Codable {
		let code: String
		let message: String
	}

	private lazy var publicKey: String = {
		Bundle.main.stringValue(for: "MARVEL_API_PUBLIC_KEY")
	}()

	private lazy var privateKey: String = {
		Bundle.main.stringValue(for: "MARVEL_API_PRIVATE_KEY")
	}()

	private let baseURL = "http://gateway.marvel.com/v1/public"

	private let urlSession: URLSession
	private let jsonDecoder: JSONDecoder = .init()

	init(urlSession: URLSession = .shared) {
		self.urlSession = urlSession
		self.jsonDecoder.dateDecodingStrategy = .iso8601
	}

	func getItems<T: Codable>(_ endpoint: APIEndpoint) async throws -> [T] {
		let request = request(for: endpoint)
		let (data, response) = try await urlSession.data(for: request) // Using async/await

		// perform basic validation
		guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
			throw APIError.invalidResponse(response)
		}
		return try decodeAPIResult(data)
	}

	private func request(for endpoint: APIEndpoint) -> URLRequest {
		var url = URL(string: baseURL)! // safe to force-unwrap hardcoded url or it wouldn't compile
		url.append(path: endpoint.path)

		// Set query params
		let timeStamp = String(Int(Date().timeIntervalSince1970))
		let tsQueryItem = URLQueryItem(name: "ts", value: timeStamp)
		let publicKeyQueryItem = URLQueryItem(name: "apikey", value: publicKey)
		let hashValue = (timeStamp + privateKey + publicKey).md5
		let hashQueryItem = URLQueryItem(name: "hash", value: hashValue)
		url.append(queryItems: [tsQueryItem, publicKeyQueryItem, hashQueryItem])

		var request = URLRequest(url: url)
		// The `Accept` header can also be set to `gzip` to save bandwidth
		request.setValue("application/json", forHTTPHeaderField: "Accept")

		// We can optionally append an "If-None-Match" header field if we have called
		// this endpoint before and can use the cached response.
		// see: https://developer.marvel.com/documentation/generalinfo
		return request
	}

	// exposed for testing
	func decodeAPIResult<T: Codable>(_ data: Data) throws -> [T] {
		do {
			// try to decode as intended type
			let result = try jsonDecoder.decode(APIResult<T>.self, from: data)
			// TODO: Paging
			return result.results
		} catch (let decodingError) {
			// try to decode the data as the Marvel API error message `{code: "", message: ""}`
			do {
				let codedMessage = try jsonDecoder.decode(CodedError.self, from: data)
				throw codedMessage
			} catch {
				// intentionally left blank to fall-through
			}
			// else re-throw the decoding error
			throw decodingError
		}
	}

}

private extension String {

	var md5: String {
		let digest = Insecure.MD5.hash(data: Data(self.utf8))
		return digest.map {
			String(format: "%02hhx", $0)
		}.joined()
	}

}

private extension Bundle {

	func stringValue(for key: String) -> String {
		guard let key = object(forInfoDictionaryKey: key) as? String,
			  !key.isEmpty else {
			fatalError("No Bundle Key for '" + key + "'")
		}
		return key
	}

}
