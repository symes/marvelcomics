// Created by Scott Symes

import Combine
import Foundation
import SwiftUI

protocol ImageServiceProtocol {
	var images: [URL: UIImage] { get }
	var imagesPublisher: Published<[URL: UIImage]>.Publisher { get }
	func fetchImages(for urls: [URL])
}

/// Service to fetch public images from a given URL and cache them in the internal dictionary
class ImageService: ObservableObject, ImageServiceProtocol {

	enum ImageError: Error {
		case invalidData
	}

	@Published var images: [URL: UIImage]

	var imagesPublisher: Published<[URL: UIImage]>.Publisher { $images }

	private var cancellables = Set<AnyCancellable>()

	private var urlSession: URLSession

	init(urlSession: URLSession = .shared) {
		self.images = [:]
		self.urlSession = urlSession
	}

	func fetchImages(for urls: [URL]) {
		for url in urls {
			guard !images.keys.contains(url) else { continue } // skip fetch if we have already cached this image
			urlSession.dataTaskPublisher(for: url) // Using Combine
				.map(\.data)
				.map(UIImage.init)
				.receive(on: DispatchQueue.main)
				.catch { error in
					// TODO: handle / log error
					print(error)
					return Just(Optional<UIImage>.none)
				}
				.sink { [weak self] in
					if let image = $0 {
						// For better performance, these images could be written to disk
						// but for the sake of simplicty, this app will fetch them on each load
						self?.images[url] = image
					}
				}
				.store(in: &cancellables)
		}
	}

}
