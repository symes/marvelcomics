// Created by Scott Symes

import Foundation

struct StoryListing: Hashable {

	enum StoryType: String, Codable {
		case cover
		case interiorStory
		case none = ""
	}

	let detailURL: URL
	let name: String
	let type: StoryType

	enum CodingKeys: String, CodingKey {
		case name, type
		case detailURL = "resourceURI"
 	}

}

// MARK: Codable Conformance
// Conform to `Codable` in an extension so the value-wise initialiser for these structs is still synthesised
// i.e. `Comic.EventDate(eventType: "Landed on Moon", date: .now)` is still valid

extension StoryListing: Codable { }
