// Created by Scott Symes

import Foundation

struct CreatorListing: Hashable {
	let detailURL: URL
	let name: String
	let role: String // enum? Not documented as such

	enum CodingKeys: String, CodingKey {
		case name, role
		case detailURL = "resourceURI"
	}
}

// MARK: Codable Conformance
// Conform to `Codable` in an extension so the value-wise initialiser for these structs is still synthesised
// i.e. `Comic.EventDate(eventType: "Landed on Moon", date: .now)` is still valid

extension CreatorListing: Codable { }
