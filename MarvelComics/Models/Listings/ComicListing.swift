// Created by Scott Symes

import Foundation

struct ComicListing: Hashable {
	let detailURL: URL
	let name: String

	enum CodingKeys: String, CodingKey {
		case name
		case detailURL = "resourceURI"
	}
}

// MARK: Codable Conformance
// Conform to `Codable` in an extension so the value-wise initialiser for these structs is still synthesised
// i.e. `Comic.EventDate(eventType: "Landed on Moon", date: .now)` is still valid

extension ComicListing: Codable { }
