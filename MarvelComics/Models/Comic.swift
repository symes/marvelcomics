// Created by Scott Symes

import Foundation

struct Comic: Identifiable, Hashable {

	let id: Int
	let digitalId: Int
	let title: String
	let resourceURI: URL
	let issueNumber: Int
	let thumbnail: ResourceURL
	let variantDescription: String?
	let description: String?
	let modified: Date?
	let isbn: String?
	let upc: String?
	let diamondCode: String?
	let ean: String?
	let issn: String?
	let format: String
	let pageCount: Int
	let textObjects: [TextObject]
	let urls: [Comic.URLListing]
	let series: SeriesListing?
	// TODO: Implement these models as required
	// let variants: []
	// let collections: []
	// let collectedIssues: []
	let dates: [EventDate]
	let prices: [Comic.Price]
	let images: [ResourceURL]
	let creators: [CreatorListing]
	let characters: [CharacterListing]
	let stories: [StoryListing]
	let events: [EventDate]
}

extension Comic {

	struct EventDate: Hashable {
		let eventType: String? // might be an enum, not specified in docs
		let date: Date?

		enum CodingKeys: String, CodingKey {
			case date
			case eventType = "type"
		}
	}

	struct Price: Hashable {
		let priceType: String
		let price: Decimal

		enum CodingKeys: String, CodingKey {
			case price
			case priceType = "type"
		}
	}

	struct TextObject: Hashable {
		let textType: String // enum?
		let language: String // enum
		let text: String

		enum CodingKeys: String, CodingKey {
			case language, text
			case textType = "type"
		}
	}

	struct URLListing: Hashable {
		let urlType: String // should be an enum
		let url: URL

		enum CodingKeys: String, CodingKey {
			case url
			case urlType = "type"
		}
	}

}

// MARK: Codable Conformance
// Conform to `Codable` in an extension so the value-wise initialiser for these structs is still synthesised
// i.e. `Comic.EventDate(eventType: "Landed on Moon", date: .now)` is still valid

extension Comic: Codable {

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.id = try container.decode(Int.self, forKey: .id)
		self.digitalId = try container.decode(Int.self, forKey: .digitalId)
		self.title = try container.decode(String.self, forKey: .title)
		self.issueNumber = try container.decode(Int.self, forKey: .issueNumber)
		self.variantDescription = try container.decodeIfPresentNotEmpty(forKey: .variantDescription)
		self.description = try container.decodeIfPresentNotEmpty(forKey: .description)
		do {
			// Some of the dates are given with a negative year which throws a DecodingError
			// rather than fail the object instatiation, we'd rather set this to `nil`
			self.modified = try container.decodeIfPresent(Date.self, forKey: .modified)
		} catch {
			self.modified = nil
		}
		self.isbn = try container.decodeIfPresentNotEmpty(forKey: .isbn)
		self.upc = try container.decodeIfPresentNotEmpty(forKey: .upc)
		self.diamondCode = try container.decodeIfPresentNotEmpty(forKey: .diamondCode)
		self.ean = try container.decodeIfPresentNotEmpty(forKey: .ean)
		self.issn = try container.decodeIfPresentNotEmpty(forKey: .issn)
		self.format = try container.decode(String.self, forKey: .format)
		self.pageCount = try container.decode(Int.self, forKey: .pageCount)
		self.textObjects = try container.decodeIfPresentNotEmpty(forKey: .textObjects) ?? []
		self.resourceURI = try container.decode(URL.self, forKey: .resourceURI)
		self.urls = try container.decodeIfPresentNotEmpty(forKey: .urls) ?? []
		self.series = try container.decodeIfPresent(SeriesListing.self, forKey: .series)
		self.dates = try container.decodeIfPresentNotEmpty(forKey: .dates) ?? []
		self.prices = try container.decodeIfPresentNotEmpty(forKey: .prices) ?? []
		self.thumbnail = try container.decode(ResourceURL.self, forKey: .thumbnail)
		self.images = try container.decodeIfPresentNotEmpty(forKey: .images) ?? []
		self.creators = try container.decodeIfPresent(CollectionResult<CreatorListing>.self, forKey: .creators)?.items ?? []
		self.characters = try container.decodeIfPresent(CollectionResult<CharacterListing>.self, forKey: .characters)?.items ?? []
		self.stories = try container.decodeIfPresent(CollectionResult<StoryListing>.self, forKey: .stories)?.items ?? []
		self.events = try container.decodeIfPresent(CollectionResult<EventDate>.self, forKey: .events)?.items ?? []
	}

}

extension Comic.EventDate: Codable {

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.eventType = try container.decodeIfPresent(String.self, forKey: .eventType)
		do {
			// Some of the dates are given with a negative year which throws a DecodingError
			// rather than fail the object initialiser, we'd rather set this to `nil`
			self.date = try container.decode(Date.self, forKey: .date)
		} catch {
			self.date = nil
		}
	}
	
}

extension Comic.Price: Codable { }

extension Comic.TextObject: Codable { }

extension Comic.URLListing: Codable { }
