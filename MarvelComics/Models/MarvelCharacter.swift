// Created by Scott Symes

import Foundation
import SwiftUI

struct MarvelCharacter: Identifiable, Hashable {

	let id: Int
	let name: String
	let lastModified: Date?
	let description: String?
	let detailURL: URL?
	let thumbnailResource: ResourceURL?
	let comics: [ComicListing]
	let series: [SeriesListing]
	let stories: [StoryListing]
	let events: [EventListing]
	let links: [LinkListing]

	enum CodingKeys: String, CodingKey {
		case id, name, description, comics, series, stories, events
		case lastModified = "modified"
		case thumbnailResource = "thumbnail"
		case detailURL = "resourceURI"
		case links = "urls"
	}

}

extension MarvelCharacter: Codable {

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.id = try container.decode(Int.self, forKey: .id)
		self.name = try container.decode(String.self, forKey: .name)
		self.lastModified = try container.decodeIfPresent(Date.self, forKey: .lastModified)
		self.description = try container.decodeIfPresentNotEmpty(forKey: .description) // No use setting an empty String
		self.detailURL = try container.decodeIfPresent(URL.self, forKey: .detailURL)
		self.thumbnailResource = try container.decodeIfPresent(ResourceURL.self, forKey: .thumbnailResource)
		self.comics = try container.decodeIfPresent(CollectionResult<ComicListing>.self, forKey: .comics)?.items ?? []
		self.series = try container.decodeIfPresent(CollectionResult<SeriesListing>.self, forKey: .series)?.items ?? []
		self.stories = try container.decodeIfPresent(CollectionResult<StoryListing>.self, forKey: .stories)?.items ?? []
		self.events = try container.decodeIfPresent(CollectionResult<EventListing>.self, forKey: .events)?.items ?? []
		self.links = try container.decodeIfPresent([LinkListing].self, forKey: .links) ?? []
	}

}

struct LinkListing: Hashable {
	let linkType: String
	let url: URL

	enum CodingKeys: String, CodingKey {
		case url
		case linkType = "type"
	}
}

// MARK: Codable Conformance
// Conform to `Codable` in an extension so the value-wise initialiser for these structs is still synthesised
// i.e. `Comic.EventDate(eventType: "Landed on Moon", date: .now)` is still valid

extension LinkListing: Codable { }
