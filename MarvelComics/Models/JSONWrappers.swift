// Created by Scott Symes

import Foundation

// Some structs reflecting how the API data is structured

struct PagedList<Result: Codable>: Codable {
	let offset: Int
	let limit: Int
	let total: Int
	let count: Int
	let results: [Result]
}

struct CollectionResult<Result: Codable>: Codable {
	let available: Int
	let collectionURI: URL
	let returned: Int
	let items: [Result]
}

struct APIResult<Result: Codable>: Codable {
	let code: Int
	let status: String
	let copyright: String
	let attributionText: String
	let attributionHTML: String
	let etag: String
	let data: PagedList<Result>
}

// MARK: Convenience Accessor

extension APIResult {

	var results: [Result] {
		data.results
	}

}
