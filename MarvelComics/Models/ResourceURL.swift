// Created by Scott Symes

import Foundation

struct ResourceURL: Hashable {
	let path: String
	let fileExtension: String

	enum CodingKeys: String, CodingKey {
		case path
		case fileExtension = "extension"
	}

	/// Used to fetch the correct size image
	/// https://developer.marvel.com/documentation/images
	enum ImageSize: String {

		// Portrait

		/// 50x75px
		case portraitSmall = "portrait_small"

		/// 100x150px
		case portraitMedium = "portrait_medium"

		/// 150x225px
		case portraitXLarge = "portrait_xlarge"

		/// 168x252px
		case portraitFantastic = "portrait_fantastic"

		/// 300x450px
		case portraitUncanny = "portrait_uncanny"

		/// 216x324px
		case portraitIncredible = "portrait_incredible"


		// Square

		/// 65x45px
		case standardSmall = "standard_small"

		/// 100x100px
		case standardMedium = "standard_medium"

		/// 140x140px
		case standardLarge = "standard_large"

		/// 200x200px
		case standardXLarge = "standard_xlarge"

		/// 250x250px
		case standardFantastic = "standard_fantastic"

		/// 180x180px
		case standardAmazing = "standard_amazing"


		// Landscape

		/// 120x90px
		case landscapeSmall = "landscape_small"

		/// 175x130px
		case landscapeMedium = "landscape_medium"

		/// 190x140px
		case landscapeLarge = "landscape_large"

		/// 270x200px
		case landscapeXLarge = "landscape_xlarge"

		/// 250x156px
		case landscapeAmazing = "landscape_amazing"

		/// 464x261px
		case landscapeIncredible = "landscape_incredible"


		// Full Size

		/// full image, constrained to 500px wide
		case detail

		/// no variant descriptor
		case fullSize = ""
	}

}

// MARK: Codable Conformance
// Conform to `Codable` in an extension so the value-wise initialiser for these structs is still synthesised
// i.e. `Comic.EventDate(eventType: "Landed on Moon", date: .now)` is still valid

extension ResourceURL: Codable { }

extension ResourceURL {

	func url(_ size: ImageSize = .fullSize) -> URL? {
		switch size {
		case .fullSize:
			return URL(string: path + "." + fileExtension)
		default:
			return URL(string: path + "/\(size.rawValue).\(fileExtension)")
		}
	}

}
