// Created by Scott Symes

import Foundation

#if DEBUG
extension MarvelCharacter {

	static var threeDeeMan: Self {
		.init(
			id: 1011334,
			name: "3-D Man",
			lastModified: .now,
			description: nil,
			detailURL: URL(string: "http://gateway.marvel.com/v1/public/characters/1011334")!,
			thumbnailResource: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
				fileExtension: "jpg"
			),
			comics: [
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/21366")!,
					name: "Avengers: The Initiative (2007) #14"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/24571")!,
					name: "Avengers: The Initiative (2007) #14 (SPOTLIGHT VARIANT)"
				),
			],
			series: [],
			stories: [],
			events: [],
			links: []
		)
	}

	static var aBomb: Self {
		.init(
			id: 1017100,
			name: "A-Bomb (HAS)",
			lastModified: Date(timeIntervalSince1970: 1379534044),
			description: "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
			detailURL: URL(string: "http://gateway.marvel.com/v1/public/characters/1017100"),
			thumbnailResource: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16",
				fileExtension: "jpg"
			),
			comics: [
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/47176")!,
					name: "FREE COMIC BOOK DAY 2013 1 (2013) #1"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40632")!,
					name: "Hulk (2008) #53"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40630")!,
					name: "Hulk (2008) #54"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40628")!,
					name: "Hulk (2008) #55"
				)
			],
			series: [],
			stories: [],
			events: [],
			links: []
		)
	}

	static var aim: Self {
		 .init(
			 id: 1009144,
			 name: "A.I.M.",
			 lastModified: try! Date("2013-10-17T14:41:30-0400", strategy: .iso8601),
			 description: "AIM is a terrorist organization bent on destroying the world.",
			 detailURL: URL(string: "http://gateway.marvel.com/v1/public/characters/1009144")!,
			 thumbnailResource: ResourceURL(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec",
				fileExtension: "jpg"
			 ),
			 comics: [
				.init(
					 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/36763")!,
					 name: "Ant-Man & the Wasp (2010) #3"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/17553")!,
				 name: "Avengers (1998) #67"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/7340")!,
				 name: "Avengers (1963) #87"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/4214")!,
				 name: "Avengers and Power Pack Assemble! (2006) #2"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/63217")!,
				 name: "Avengers and Power Pack (2017) #3"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/63218")!,
				 name: "Avengers and Power Pack (2017) #4"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/63219")!,
				 name: "Avengers and Power Pack (2017) #5"
				 ),
				 .init(
				 detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/63220")!,
				 name: "Avengers and Power Pack (2017) #6"
				 )
			 ],
			 series: [],
			 stories: [],
			 events: [],
			 links: []
		 )
	}

	static var aaronStack: Self {
		.init(
			id: 1010699,
			name: "Aaron Stack",
			lastModified: try! Date("1969-12-31T19:00:00-0500", strategy: .iso8601),
			description: "",
			detailURL: URL(string: "http://gateway.marvel.com/v1/public/characters/1010699")!,
			thumbnailResource: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
				fileExtension: "jpg"
			),
			comics: [
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40776")!,
					name: "Dark Avengers (2012) #177"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40773")!,
					name: "Dark Avengers (2012) #179"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40774")!,
					name: "Dark Avengers (2012) #180"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40778")!,
					name: "Dark Avengers (2012) #181"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40787")!,
					name: "Dark Avengers (2012) #182"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/40786")!,
					name: "Dark Avengers (2012) #183"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/38073")!,
					name: "Hulk (2008) #43"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/11910")!,
					name: "Universe X (2000) #6"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/11911")!,
					name: "Universe X (2000) #7"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/11912")!,
					name: "Universe X (2000) #8"
				)],
			series: [],
			stories: [],
			events: [],
			links: []
		)
	}

	static var abomination: Self {
		.init(
			id: 1009146,
			name: "Abomination (Emil Blonsky)",
			lastModified: try! Date("2012-03-20T12:32:12-0400", strategy: .iso8601),
			description: "Formerly known as Emil Blonsky, a spy of Soviet Yugoslavian origin working for the KGB, the Abomination gained his powers after receiving a dose of gamma radiation similar to that which transformed Bruce Banner into the incredible Hulk.",
			detailURL: URL(string: "http://gateway.marvel.com/v1/public/characters/1009146")!,
			thumbnailResource: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/9/50/4ce18691cbf04",
				fileExtension: "jpg"
			),
			comics: [
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/88869")!,
					name: "Abominations (1996) #1"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/88870")!,
					name: "Abominations (1996) #2"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/88871")!,
					name: "Abominations (1996) #3"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/17547")!,
					name: "Avengers (1998) #61"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/17548")!,
					name: "Avengers (1998) #62"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/1098")!,
					name: "Avengers Vol. 1: World Trust (Trade Paperback)"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/8557")!,
					name: "Earth X (1999) #7"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/4241")!,
					name: "EARTH X TPB [NEW PRINTING] (Trade Paperback)"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/20863")!,
					name: "Hulk (2008) #3"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/2499")!,
					name: "Hulk: Destruction (2005) #4"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/14424")!,
					name: "Hulk (1999) #24"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/14425")!,
					name: "Hulk (1999) #25"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/14428")!,
					name: "Hulk (1999) #28"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/14450")!,
					name: "Hulk (1999) #50"
				),
				.init(
					detailURL: URL(string: "http://gateway.marvel.com/v1/public/comics/14451")!,
					name: "Hulk (1999) #51"
				)],
			series: [],
			stories: [],
			events: [],
			links: []
		)
	}

	static let all: [MarvelCharacter] = [
		.threeDeeMan,
		.aBomb,
		.aim,
		.aaronStack,
		.abomination,
	]

}
#endif
