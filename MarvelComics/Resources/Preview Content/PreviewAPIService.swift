// Created by Scott Symes

import Foundation

class PreviewAPIService: APIProtocol {

	func getItems<T>(_ endpoint: APIEndpoint) async throws -> [T] where T : Decodable, T : Encodable {
		switch endpoint {
		case .characters:
			return ([MarvelCharacter.threeDeeMan, .aBomb, .aim, .aaronStack, .abomination] as? [T]) ?? []
		case .comics:
			return (Comic.comics as? [T]) ?? []
		}
	}

}
