// Created by Scott Symes

import Foundation

#if DEBUG
extension Comic {

	static var comics: [Comic] {
		[
			.initiative22506,
			.initiative22300,
			.initiative22299,
			.initiative21975,
			.initiative21741,
			.initiative21546,
			.initiative21366,
			.initiative24571,
			.initiative8500,
			.initiative10225,
			.initiative10224,
			.initiative10223,
		]
	}

	static var initiative22506: Comic {
		preview(
			id: 22506,
			title: "Avengers: The Initiative (2007) #19",
			url: "http://gateway.marvel.com/v1/public/comics/22506",
			issueNo: 19,
			modified: try! Date("2015-10-27T16:38:23-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative22300: Comic {
		preview(
			id: 22300,
			title: "Avengers: The Initiative (2007) #18 (ZOMBIE VARIANT)",
			url: "http://gateway.marvel.com/v1/public/comics/22300",
			issueNo: 18,
			modified: try! Date("-0001-11-30T00:00:00-0500", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/1/10/4e94a23255996",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative22299: Comic {
		preview(
			id: 22299,
			title: "Avengers: The Initiative (2007) #18",
			url: "http://gateway.marvel.com/v1/public/comics/22299",
			issueNo: 18,
			modified: try! Date("2014-08-05T14:09:33-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/6/20/58dd057d304d1",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative21975: Comic {
		preview(
			id: 21975,
			title: "Avengers: The Initiative (2007) #17",
			url: "http://gateway.marvel.com/v1/public/comics/21975",
			issueNo: 17,
			modified: try! Date("2014-08-05T14:09:31-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/b/a0/58dd03dc2ec00",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative21741: Comic {
		preview(
			id: 21741,
			title: "Avengers: The Initiative (2007) #16",
			url: "http://gateway.marvel.com/v1/public/comics/21741",
			issueNo: 16,
			modified: try! Date("2014-08-05T14:09:28-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative21546: Comic {
		preview(
			id: 21546,
			title: "Avengers: The Initiative (2007) #15",
			url: "http://gateway.marvel.com/v1/public/comics/21546",
			issueNo: 15,
			modified: try! Date("2014-08-05T14:09:27-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/f/c0/58dbda827bec8",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative21366: Comic {
		preview(
			id: 21366,
			title: "Avengers: The Initiative (2007) #14",
			url: "http://gateway.marvel.com/v1/public/comics/21366",
			issueNo: 14,
			modified: try! Date("2014-08-05T14:09:26-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/c/60/58dbce634ea70",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative24571: Comic {
		preview(
			id: 24571,
			title: "Avengers: The Initiative (2007) #14 (SPOTLIGHT VARIANT)",
			url: "http://gateway.marvel.com/v1/public/comics/24571",
			issueNo: 14,
			modified: try! Date("-0001-11-30T00:00:00-0500", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/a/30/4e948fb5e9b52",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative8500: Comic {
		preview(
			id: 8500,
			title: "Deadpool (1997) #44",
			url: "http://gateway.marvel.com/v1/public/comics/8500",
			issueNo: 44,
			modified: try! Date("2013-06-05T11:24:31-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/b/80/4f206cc0ac28a",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative10225: Comic {
		preview(
			id: 10225,
			title: "Marvel Premiere (1972) #37",
			url: "http://gateway.marvel.com/v1/public/comics/10225",
			issueNo: 37,
			modified: try! Date("-0001-11-30T00:00:00-0500", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative10224: Comic {
		preview(
			id: 10224,
			title: "Marvel Premiere (1972) #36",
			url: "http://gateway.marvel.com/v1/public/comics/10224",
			issueNo: 36,
			modified: try! Date("-0001-11-30T00:00:00-0500", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
				fileExtension: "jpg"
			)
		)
	}

	static var initiative10223: Comic {
		preview(
			id: 10223,
			title: "Marvel Premiere (1972) #35",
			url: "http://gateway.marvel.com/v1/public/comics/10223",
			issueNo: 35,
			modified: try! Date("2023-04-05T17:42:17-0400", strategy: .iso8601),
			thumbnail: .init(
				path: "http://i.annihil.us/u/prod/marvel/i/mg/6/60/642ddeb849005",
				fileExtension: "jpg"
			)
		)
	}

	private static func preview(
		id: Int,
		title: String,
		url: String,
		issueNo: Int,
		modified: Date,
		thumbnail: ResourceURL
	) -> Comic {
		Comic(
			id: id,
			digitalId: id,
			title: title,
			resourceURI: URL(string: url)!,
			issueNumber: issueNo,
			thumbnail: thumbnail,
			variantDescription: nil,
			description: nil,
			modified: modified,
			isbn: nil,
			upc: nil,
			diamondCode: nil,
			ean: nil,
			issn: nil,
			format: "comic",
			pageCount: 32,
			textObjects: [],
			urls: [],
			series: nil,
			dates: [],
			prices: [],
			images: [],
			creators: [],
			characters: [],
			stories: [],
			events: []
		)
	}

}
#endif
