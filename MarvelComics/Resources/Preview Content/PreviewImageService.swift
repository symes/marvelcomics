// Created by Scott Symes

import Foundation
import SwiftUI

class PreviewImageService: ImageServiceProtocol {

	var imagesPublisher: Published<[URL: UIImage]>.Publisher {
		$images
	}

	func fetchImages(for urls: [URL]) {
		// no-op, dictionary pre-populated
	}

	@Published var images: [URL: UIImage]

	init(_ images: [URL: UIImage]) {
		self.images = images
	}

}

extension Dictionary where Key == URL, Value == UIImage {

	static let characters = [
		MarvelCharacter.threeDeeMan.thumbnailResource!.url(.standardLarge)!: UIImage(imageLiteralResourceName: "1011334_3D-Man"),
		MarvelCharacter.aBomb.thumbnailResource!.url(.standardLarge)!: UIImage(imageLiteralResourceName: "1017100_A-Bomb"),
		MarvelCharacter.aaronStack.thumbnailResource!.url(.standardLarge)!: UIImage(imageLiteralResourceName: "1009148_Absorbing-Man"),
		MarvelCharacter.abomination.thumbnailResource!.url(.standardLarge)!: UIImage(imageLiteralResourceName: "1009146_Abomination-(Emil-Blonsky)"),
		MarvelCharacter.aim.thumbnailResource!.url(.standardLarge)!: UIImage(imageLiteralResourceName: "1009144_AIM"),
	]

	static let comics = [
		MarvelCharacter.threeDeeMan.thumbnailResource!.url(.landscapeIncredible)!: UIImage(imageLiteralResourceName: "1011334_3D-Man"),
		MarvelCharacter.aBomb.thumbnailResource!.url(.landscapeIncredible)!: UIImage(imageLiteralResourceName: "1017100_A-Bomb"),
		MarvelCharacter.aaronStack.thumbnailResource!.url(.landscapeIncredible)!: UIImage(imageLiteralResourceName: "1009148_Absorbing-Man"),
		MarvelCharacter.abomination.thumbnailResource!.url(.landscapeIncredible)!: UIImage(imageLiteralResourceName: "1009146_Abomination-(Emil-Blonsky)"),
		MarvelCharacter.aim.thumbnailResource!.url(.landscapeIncredible)!: UIImage(imageLiteralResourceName: "1009144_AIM"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative16"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/1/10/4e94a23255996/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative18"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/6/20/58dd057d304d1/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative18-1"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/b/a0/58dd03dc2ec00/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative17"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/c/10/58dd01dbc6e51/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative16-1"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/f/c0/58dbda827bec8/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative15"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/c/60/58dbce634ea70/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative14"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/a/30/4e948fb5e9b52/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Initiative14-1"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/b/80/4f206cc0ac28a/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Deadpool44"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "image_not_available"),
		URL(string: "http://i.annihil.us/u/prod/marvel/i/mg/6/60/642ddeb849005/portrait_fantastic.jpg")!: UIImage(imageLiteralResourceName: "Premiere35"),
	]

	static var all: Self {
		comics.merging(characters, uniquingKeysWith: { first, _ in return first })
	}

}
