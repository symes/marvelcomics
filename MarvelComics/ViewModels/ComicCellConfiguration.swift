// Created by Scott Symes

import SwiftUI

struct ComicCellConfiguration {

	private let comic: Comic
	let image: UIImage?

	init(comic: Comic, image: UIImage?) {
		self.comic = comic
		self.image = image
	}

	var displayDate: String? {
		comic.displayDate
	}

	var issueNumber: String {
		let format = NSLocalizedString("Comic.issueFormat", bundle: .main, comment: "")
		return String(format: format, comic.issueNumber)
	}

	var title: String {
		comic.title
	}

}

private extension Comic {

	var displayDate: String? {
		guard let date = self.modified else { return nil }
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		formatter.timeStyle = .none
		return formatter.string(from: date)
	}

}
