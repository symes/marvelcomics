// Created by Scott Symes

import Combine
import Foundation
import SwiftUI

@MainActor
class CharactersViewModel: ObservableObject {

	@Published var characters: [MarvelCharacter] = []
	@Published var images: [URL: UIImage]

	private(set) var apiService: APIProtocol
	private(set) var imageService: ImageServiceProtocol

	var error: Error?
	private var imagesCancellable: AnyCancellable?
	private let characterImageSize: ResourceURL.ImageSize = .standardLarge

	// In lieu of Direct Injection, we'll instantiate with (default) instances
	init(
		apiService: APIProtocol = APIService(),
		imageService: ImageServiceProtocol = ImageService()
	) {
		self.apiService = apiService
		self.imageService = imageService
		self.images = imageService.images
		imagesCancellable = imageService
			.imagesPublisher
			.sink {
				self.images = $0
			}
	}

	func fetchCharacterList() {
		Task { [weak self] in
			do {
				let characters: [MarvelCharacter] = try await apiService.getItems(.characters)
				let thumbnailURLs = characters
					.compactMap(\.thumbnailResource)
					.compactMap { $0.url(characterImageSize) }
				self?.imageService.fetchImages(for: thumbnailURLs)
				// Sort by popularity, defined as 'most appearances in comics'
				self?.characters = characters.sorted { $0.comics.count > $1.comics.count }
			} catch {
				self?.error = error
				// TODO: Handle / Log error
			}
		}
	}

	func image(for character: MarvelCharacter) -> UIImage? {
		guard let url = character.thumbnailResource?.url(characterImageSize) else { return nil }
		return images[url]
	}

	var heroImage: Image {
		Image("MARVEL")
	}

	var listHeadingText: String {
		NSLocalizedString("Characters.listHeading", comment: "").uppercased()
	}

	var seeAllButtonText: String {
		NSLocalizedString("Characters.seeAllButton", comment: "")
	}

	var attributionText: String {
		NSLocalizedString("Attribution", comment: "")
	}
}
