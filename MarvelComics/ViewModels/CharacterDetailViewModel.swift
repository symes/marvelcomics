// Created by Scott Symes

import Combine
import Foundation
import SwiftUI

@MainActor
class CharacterDetailViewModel: ObservableObject {

	let character: MarvelCharacter

	private let apiService: APIProtocol
	private let imageService: ImageServiceProtocol
	private var imagesCancellable: AnyCancellable?
	private var comicThumbnails: [URL] = []

	@Published private(set) var comics: [Comic] = []
	@Published var images: [URL: UIImage]
	var error: Error?

	private let characterBackgroundSize: ResourceURL.ImageSize = .landscapeIncredible
	private let comicThumbnailSize: ResourceURL.ImageSize = .portraitFantastic

	init(
		character: MarvelCharacter,
		apiService: APIProtocol = APIService(),
		imageService: ImageServiceProtocol = ImageService()
	) {
		self.character = character
		self.apiService = apiService
		self.imageService = imageService
		self.images = imageService.images
		imagesCancellable = imageService
			.imagesPublisher
			.sink {
				self.images = $0
			}
		if let url = character.thumbnailResource?.url(characterBackgroundSize) {
			imageService.fetchImages(for: [url])
		}
		fetchComicImages()
	}

	private func fetchComicImages() {
		Task { [weak self] in
			guard let self else { return }
			do {
				self.comics = try await self.apiService.getItems(.comics(character))
				comicThumbnails = self.comics.map(\.thumbnail).compactMap { $0.url(comicThumbnailSize) }
				self.imageService.fetchImages(for: comicThumbnails)
			} catch {
				self.error = error
				// TODO: handle / log error
			}
		}
	}

	var thumbnailImage: UIImage? {
		guard let url = character.thumbnailResource?.url(characterBackgroundSize) else { return nil }
		return imageService.images[url]
	}

	func image(for comic: Comic) -> UIImage? {
		guard let url = comic.thumbnail.url(comicThumbnailSize) else { return nil }
		return images[url]
	}

	var allImagesLoaded: Bool {
		guard !comicThumbnails.isEmpty else { return false }
		return Set(comicThumbnails).isStrictSubset(of: images.keys)
	}

	var attributionText: String {
		NSLocalizedString("Attribution", comment: "")
	}

	struct FollowButtonConfiguration {

		private let isFollowing: Bool

		init(isFollowing: Bool) {
			self.isFollowing = isFollowing
		}

		var buttonText: String {
			let localizationKey = isFollowing ? "Character.following" : "Character.notFollowing"
			return NSLocalizedString(localizationKey, bundle: .main, comment: "")
		}

		var buttonImage: Image {
			let imageName = isFollowing ? "star.fill" : "star"
			return Image(systemName: imageName)
		}

	}


}
